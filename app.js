const express = require('express');
const app = new express();
var mongoose = require('mongoose');

//Importing routes
const userRoutes = require('./routers/userRoute');
const postRoutes = require('./routers/postRoute');
const reteriveRoutes = require('./routers/reteriveRoute'); 

//Cnnection with database
mongoose.connect("mongodb://akash_goindi:akashgoindi@cluster0-shard-00-00-ekaca.mongodb.net:27017,cluster0-shard-00-01-ekaca.mongodb.net:27017,cluster0-shard-00-02-ekaca.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true",{useNewUrlParser: true});
// mongodb+srv://<username>:<password>@cluster0-sdven.mongodb.net/test?retryWrites=true


// CORS Headers
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  if (req.method === 'OPTIONS') {
      res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
      return res.status(200).json({});
  }
  next();
});


//Body Parser Code
app.use(express.urlencoded({extended: false}));
app.use(express.json());﻿

//Setting routes Middleware
app.use('/user',userRoutes);
app.use('/post',postRoutes);
app.use('/',reteriveRoutes)

module.exports = app;