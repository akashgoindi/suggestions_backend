var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');

var verifyToken = require('../TokenVerification');
var User = require('../models/userModel');
var Post = require('../models/postModel');


//........................AUTH.................
//Signup api
router.post('/signup',(req,res)=>{
  //Check if email already exist or not
  User.find({email:req.body.email})
  .exec()
  .then((data)=>{
    if(data && data.length){
      res.status(500).json({
      message:"That Email id is already taken ! Try another one..."
      })
    }
    else{
      //Hash the password
      bcrypt.hash(req.body.password, 8).then((hash)=>{
      //Create new user
      var newUser = new User({
        email: req.body.email,
        //save hashed password in db
        password: hash,
        username: req.body.username,
        connections:[]
      })
      //Save user in db
      newUser.save().then((user)=>{
        res.status(200).json({
          message:"Account Created !!!",
          user
        })
      }).catch(err=>{
        res.status(500).json({
          error:err.message
        })
      })
      })
      //If hashing failed then...
      .catch((err)=>{
        ress.send(500).json({
          message:"Sorry can not create new account"
        })
      })
    }
  }).catch(err=>{
    res.status(500).json({
      message:err.message
    })
  })
})

//Login api
router.post('/login',(req,res,next)=>{
  //Check if user exists with that email
  User.find({email:req.body.email})
  .exec()
  .then((user)=>{
    //compare stores encrypted password
     bcrypt.compare(req.body.password,user[0].password,(err,result)=>{
       if(err){
         res.status(401).json({
           message:"Authentication Failed"
         })
       }
       if(result){
         // create a token with following properties and a key 
         jwt.sign({
           username:user[0].username,
           email:user[0].email,
           _id:user[0]._id,
         },'honeyGoindi',(err,token)=>{
           if(err){
             res.status(401).json({
               message:"Authentication Failed"
             })
           }
           else{
             //send response
              res.status(200).json({
                message:"Autherization Successful",
                token,
                username:user[0].username,
                email:user[0].email,
                _id:user[0]._id,
                connections:user[0].connections
              })
            } 
         })
       }
       else{
         res.status(401).json({
           message:"Authentication Failed"
         })
       }
     })
  })
  .catch((err)=>{
    res.status(401).json({
      message:"Authentication Failed"
    })
  })
})


module.exports = router;
