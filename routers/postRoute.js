var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();

var verifyToken = require('../TokenVerification');
var Post = require('../models/postModel');
var User = require('../models/userModel');

//post  call to add new post
router.post('/add_post',verifyToken,(req,res,next)=>{

  var newPost = new Post({
     user:req.userData._id,
     username:req.userData.username,
     title:req.body.title,
     description:req.body.description,
     postedOn:new Date(),
     likes:[],
     comments:[]
  })
  newPost.save()
  .then((post)=>{
    res.status(200).json({
      message:"Post Successfully Saved",
      post
    })
  })
  .catch((err)=>{
    res.status(500).json({
      message:"Can't add post at the moment",
      error:err.message
    })
  })
})

//update like counter
router.put('/update/:id',verifyToken,(req,res)=>{
   Post.findOne({_id : req.params.id})
  .exec()
  .then((post)=>{
     var temp = post.likes
     var liked = false

     temp.forEach((val)=>{
       if(val == req.userData._id){
          liked = true

       }
      })
      if(liked){
        var index = temp.findIndex((position)=>{
          position == req.userData._id
        })
        temp.splice(index,1)
      }
      else{
        temp.push(req.userData._id)
      }
    post.likes = temp;
    post.save()
    .then((doc)=>{
      res.send(doc)
    })
     .catch((err)=>{
      req.status(500).send({error:err.message})
    })
  })
  .catch((err)=>{
    res.status(404).json({
     message:"Cant find Post",
     error:err.message
    })
  })
})

//add Comments 
router.put('/comment/:id',verifyToken,(req,res)=>{
   Post.findOne({_id : req.params.id})
  .exec()
  .then((post)=>{
     var temp = post.comments
     post.comments.push({
       userid:req.userData._id,
       username:req.userData.username,
       message:req.body.message
     })
    post.save()
    .then((doc)=>{
      res.send(doc)
    })
     .catch((err)=>{
      req.status(500).send({error:err.message})
    })
  })
  .catch((err)=>{
    res.status(404).json({
     message:"Cant find Post",
     error:err.message
    })
  })
})


//Delete Post 
router.delete('/remove/:id',verifyToken,(req,res)=>{
   Post.findOne({_id : req.params.id})
  .exec()
  .then((post)=>{
    post.remove()
    .then((post)=>{
      res.status(200).json(post)
    })
    .catch(err=>{
      res.status(500).json({
        message:"Can't delete post",
        error:err.message
      })
    })
  })
  .catch((err)=>{
    res.status(500).json({
     message:"Can't delete Post",
     error:err.message
    })
  })
})



module.exports = router;