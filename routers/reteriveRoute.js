var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();

var verifyToken = require('../TokenVerification');
var Post = require('../models/postModel');
var User = require('../models/userModel');

//get All posts of a user
  router.get('/user/:id',(req,res)=>{
    User.findOne({_id : req.params.id})
    .sort({'postedOn':'desc'})
    .exec()
    .then((user)=>{
       Post.find({user:req.params.id})
      .sort({'postedOn':'desc'})
      .exec()
      .then((posts)=>{
          res.status(200).json({
            posts:posts,
            userDetails:{
              email:user.email,
              username: user.username,
              id:user._id
            }
          })
      })
      .catch((err)=>{
        res.status(404).json({
          message:"Cant get posts at the moment",
          error:err.message
        })
      })
    })
    .catch((err)=>{
      res.status(500).json({
        message:"Can't rech out to user",
        error:err.message
      })
    })
  })

  //Get a post by Id
router.get('/:id',(req,res)=>{
  Post.findOne({_id : req.params.id})
  .exec()
  .then((post)=>{
    if(post){
      res.status(200).json({post})
    }
    else{
        res.status(200).json({post:[]})
    }
  })
  .catch((err)=>{
    res.status(404).json({
      message:"Cant find user",
      error:err.message
    })
  })
})

//get all posts
router.get('/',(req,res)=>{
  Post.find()
  .sort({'postedOn':'desc'})
  .exec()
  .then((posts)=>{
    if(posts && posts.length){
      res.status(200).json(posts)
    }
    else{
      res.json({
        message:"No Posts"
      })
    }
  })
  .catch((err)=>{
    res.status(500).json({
      message:"Cant fetch posts at the moment",
      error:err.message
    })
  })
})


//get Details of user who has logged in
router.post('/myAccount',verifyToken,(req,res)=>{
  User.findOne({_id:req.userData._id})
  .exec()
  .then((user)=>{
     Post.find({user : req.userData._id})
  .sort({'postedOn':'desc'})
  .exec()
  .then((posts)=>{
     if(posts && posts.length){
      res.status(200).json({
        posts:posts,
        userDetails:{
          email:user.email,
          username: user.username,
          id:user._id
        }
      })
    }
    else{
      res.json({
        message:"No Posts"
      })
    }
  })
  .catch((err)=>{
    res.status(404).json({
      message:"Cant not find required data",
      error:err.message
    })
  })
  })
  .catch(err=>{
    res.status(500).json({
      message:"Cant get user Details",
      error:err.message
    })
  })
 
})

module.exports = router;