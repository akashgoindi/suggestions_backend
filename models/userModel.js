var mongoose = require('mongoose');

// Schema for User Model
const userSchema = mongoose.Schema({
  email:{ type:String, required:true },
  password:{ type:String, required:true},
  username:{type:String, required:true },
  connections:[]
})

module.exports = mongoose.model('User',userSchema);