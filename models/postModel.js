var mongoose = require('mongoose');

var postSchema = mongoose.Schema({
  user: {type:mongoose.Schema.Types.ObjectId, ref:"User", required:true},
  username:{type:String, required:true},
  title: { type:String, required: true },
  description: { type:String, required: true},
  postedOn: Object,
  likes: [],
  comments:[]
})

module.exports = mongoose.model('Post',postSchema)